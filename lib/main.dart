import 'package:flutter/material.dart';
import 'package:speech_recognition/speech_recognition.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(SpeechRecognitionApp());
}

class SpeechRecognitionApp extends StatefulWidget {
  @override
  _SpeechRecognitionState createState() => _SpeechRecognitionState();
}

class _SpeechRecognitionState extends State<SpeechRecognitionApp> {
  final _transcriptionController = TextEditingController();
  SpeechRecognition _speech;
  bool _speechRecognitionAvailable = false;
  bool _isListening = false;
  bool _isStopped = false;
  bool _isCanceled = false;
  String _transcription = '';
  String _bufferTranscription = '';

  @override
  initState() {
    super.initState();
    _activateSpeechRecognizer();
  }

  void _activateSpeechRecognizer() {
    _speech = SpeechRecognition();
    _speech.setAvailabilityHandler(_onSpeechAvailability);
    _speech.setRecognitionResultHandler(_onRecognitionResult);
    _speech.setRecognitionCompleteHandler(_onRecognitionComplete);
    _speech.activate().then((res) {
      setState(() => _speechRecognitionAvailable = res);
    });
  }

  @override
  Widget build(BuildContext context) {
    _transcriptionController.text = _transcription;
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('SpeechRecognition')),
        body: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text("Buffer: $_bufferTranscription"),
              Container(
                padding: const EdgeInsets.all(8.0),
                color: Colors.grey.shade200,
                child: TextFormField(
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  onChanged: (text) {
                    _transcription = text;
                  },
                  controller: _transcriptionController,
                ),
              ),
              _buildButton(
                onPressed: _speechRecognitionAvailable && !_isListening ? () => _start() : null,
                label: _isListening ? 'Listening...' : 'Listen',
              ),
              _buildButton(
                onPressed: _isListening ? () => _cancel() : null,
                label: 'Cancel',
              ),
              _buildButton(
                onPressed: _isListening ? () => _stop() : null,
                label: 'Stop',
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildButton({String label, VoidCallback onPressed}) => Padding(
      padding: EdgeInsets.all(12.0),
      child: RaisedButton(
        color: Colors.blueAccent,
        onPressed: onPressed,
        child: Text(
          label,
          style: const TextStyle(color: Colors.white),
        ),
      ));

  void _start() async {
    PermissionStatus status = await PermissionHandler().checkPermissionStatus(PermissionGroup.speech);
    if (status == PermissionStatus.granted) {
      _speech.listen(locale: "en_US");
    } else {
      await PermissionHandler().requestPermissions([PermissionGroup.speech]);
    }
  }

  void _cancel() => _speech.cancel().then((result) {
        setState(() {
          _isCanceled = true;
          _isListening = !result;
        });
      });

  void _stop() => _speech.stop().then((result) {
        setState(() {
          _isStopped = true;
          _isListening = !result;
        });
      });

  void _onSpeechAvailability(bool result) {
    setState(() => _isListening = result);
  }

  void _onRecognitionResult(String text) {
    setState(() {
      if (!_isStopped) {
        _bufferTranscription = text;
      }
    });
  }

  void _onRecognitionComplete() => setState(() {
        if (!_isCanceled && _bufferTranscription.isNotEmpty) {
          _transcription.trimRight();
          _transcription += "$_bufferTranscription ";
        }
        _isListening = false;
        _isCanceled = false;
        _isStopped = false;
        _bufferTranscription = '';
      });
}
